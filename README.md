# hifigan vocoder

- `brain_idl` version: 1.0.0
- mel-spectrogram: nvidia mel. 자세한 내용은 https://pms.maum.ai/confluence/x/o0iqAQ 참조
- 관련 컨플루언스 글: TBD

Implementation of https://arxiv.org/abs/2010.05646, based on https://github.com/jik876/hifi-gan.
- Input: mel-spectrogram (see `mel_spectrogram` module in `datasets/mel.py`)
- Output: raw audio

## Build

```bash
docker build -f Dockerfile -t hifigan:<version> .
docker build -f Dockerfile-server --build-arg hifigan_version=<version> -t hifigan:<version>-server .
```

## Inference (Serving)

```bash
docker run -d \
--gpus '"device=0"' \
-v <checkpoint_directory>:/model \
-p <port>:35003 \
-e HIFIGAN_CONFIG=/model/<path_to_config_yaml> \
-e HIFIGAN_MODEL=/model/<path_to_checkpoint> \
--name <container_name> \
hifigan:<version>
```

## Training

`python trainer.py -c <config file> -n <train name> -p <load_from_checkpoint>`

## RUN Server

`python server.py -c <config file> -m <model checkpoint path>`

## RUN client

`python client.py`

## Author

브레인 김강욱 연구원