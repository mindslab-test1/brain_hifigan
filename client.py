import grpc
import argparse
import google.protobuf.empty_pb2 as empty

from tts_pb2 import MelSpectrogram
from tts_pb2_grpc import VocoderStub


class HifiganClient(object):
    def __init__(self, remote='127.0.0.1:35003'):
        channel = grpc.insecure_channel(remote)
        self.stub = VocoderStub(channel)

    def mel2wav(self, mel):
        return self.stub.Mel2Wav(mel)

    def stream_mel2wav(self, mel_iterator):
        yield from self.stub.StreamMel2Wav(mel_iterator)

    def get_mel_config(self):
        return self.stub.GetMelConfig(empty.Empty())


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='HiFi-GAN vocoder client')
    parser.add_argument('-r', '--remote', type=str, default='127.0.0.1:35003',
                        help="grpc: ip:port")
    args = parser.parse_args()

    client = HifiganClient(remote=args.remote)

    mel_config = client.get_mel_config()
    print(mel_config)

    wav_data = client.mel2wav(
        MelSpectrogram(data=[-6.0 for i in range(80*88)])
    )
    for data in wav_data:
        print(data.data)

    mel_iterator = (MelSpectrogram(data=[-6.0 for i in range(80*88)]) for _ in range(5))
    wav_generator = client.stream_mel2wav(mel_iterator)
    for data in wav_generator:
        print(data.data)