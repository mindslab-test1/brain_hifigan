import os
import math
import tqdm
import torch
import torch.nn as nn
import torch.nn.functional as F
import itertools
import traceback
import time
from datasets.mel import mel_spectrogram

from models import Generator, MultiPeriodDiscriminator, MultiScaleDiscriminator, feature_loss, generator_loss,\
    discriminator_loss
from .utils import get_commit_hash
from .validation import validate


def train(args, pt_dir, chkpt_path, trainloader, valloader, writer, logger, hp, hp_str, g_path = None):
    model_g = Generator(hp).cuda()
    mpd = MultiPeriodDiscriminator().cuda()
    msd = MultiScaleDiscriminator().cuda()

    optim_g = torch.optim.AdamW(model_g.parameters(),
        lr=hp.train.adam.lr, betas=(hp.train.adam.beta1, hp.train.adam.beta2))
    optim_d = torch.optim.AdamW(itertools.chain(msd.parameters(), mpd.parameters()),
        lr=hp.train.adam.lr, betas=(hp.train.adam.beta1, hp.train.adam.beta2))

    githash = get_commit_hash()

    init_epoch = -1
    step = 0
    G_train = True

    if chkpt_path is not None:
        logger.info("Resuming from checkpoint: %s" % chkpt_path)
        checkpoint = torch.load(chkpt_path)
        model_g.load_state_dict(checkpoint['model_g'])
        mpd.load_state_dict(checkpoint['mpd'])
        msd.load_state_dict(checkpoint['msd'])
        optim_g.load_state_dict(checkpoint['optim_g'])
        optim_d.load_state_dict(checkpoint['optim_d'])
        step = checkpoint['step']
        init_epoch = checkpoint['epoch']

        if hp_str != checkpoint['hp_str']:
            logger.warning("New hparams is different from checkpoint. Will use new.")

        if githash != checkpoint['githash']:
            logger.warning("Code might be different: git hash is different.")
            logger.warning("%s -> %s" % (checkpoint['githash'], githash))

    elif g_path is not None:
        G_train = False
        logger.info("Loading Pretrained Generator from checkpoint: %s" % g_path)
        checkpoint = torch.load(g_path)
        model_g.load_state_dict(checkpoint['generator'])
    else:
        logger.info("Starting new training run.")

    # this accelerates training when the size of minibatch is always consistent.
    # if not consistent, it'll horribly slow down.
    torch.backends.cudnn.benchmark = True

    scheduler_g = torch.optim.lr_scheduler.ExponentialLR(optim_g, gamma=hp.train.adam.lamb, last_epoch=init_epoch)
    scheduler_d = torch.optim.lr_scheduler.ExponentialLR(optim_d, gamma=hp.train.adam.lamb, last_epoch=init_epoch)

    try:
        model_g.train()
        mpd.train()
        msd.train()

        # initial validation
        # for sanity check, and logging baseline's quality
        with torch.no_grad():
            validate(hp, args, model_g, mpd, msd, valloader, writer, init_epoch)

        for epoch in itertools.count(init_epoch+1):

            loader = tqdm.tqdm(trainloader, desc='Loading train data')
            for (melG, audioG), (melD, audioD) in loader:

                melG = melG.cuda()
                audioG = audioG.cuda()
                melD = melD.cuda()
                audioD = audioD.cuda()

                # generator
                if G_train:
                    optim_g.zero_grad()

                    fake_audio = model_g(melG)

                    _, y_df_hat_g, fmap_f_r, fmap_f_g = mpd(audioG, fake_audio)
                    _, y_ds_hat_g, fmap_s_r, fmap_s_g = msd(audioG, fake_audio)
                    loss_fm_f = feature_loss(fmap_f_r, fmap_f_g)
                    loss_fm_s = feature_loss(fmap_s_r, fmap_s_g)
                    loss_gen_f, losses_gen_f = generator_loss(y_df_hat_g)
                    loss_gen_s, losses_gen_s = generator_loss(y_ds_hat_g)

                    score_loss = loss_gen_s + loss_gen_f
                    feat_loss = (loss_fm_s + loss_fm_f) * hp.loss.feat_match

                    mel_fake = mel_spectrogram(fake_audio.squeeze(1), hp.audio.filter_length, hp.audio.n_mel_channels,
                                  hp.audio.sampling_rate,
                                  hp.audio.hop_length,
                                  hp.audio.win_length,
                                  hp.audio.mel_fmin,
                                  hp.audio.mel_fmax, center=False)
                    mel_real = mel_spectrogram(audioG.squeeze(1), hp.audio.filter_length, hp.audio.n_mel_channels,
                                  hp.audio.sampling_rate,
                                  hp.audio.hop_length,
                                  hp.audio.win_length,
                                  hp.audio.mel_fmin,
                                  hp.audio.mel_fmax, center=False)
                    mel_loss = F.l1_loss(mel_real, mel_fake) * hp.loss.mel

                    loss_g = score_loss + feat_loss + mel_loss

                    loss_g.backward()
                    optim_g.step()

                # discriminator
                fake_audio = model_g(melD)

                optim_d.zero_grad()

                # MPD
                y_df_hat_r, y_df_hat_g, _, _ = mpd(audioD, fake_audio.detach())
                loss_disc_f, losses_disc_f_r, losses_disc_f_g = discriminator_loss(y_df_hat_r, y_df_hat_g)

                # MSD
                y_ds_hat_r, y_ds_hat_g, _, _ = msd(audioD, fake_audio.detach())
                loss_disc_s, losses_disc_s_r, losses_disc_s_g = discriminator_loss(y_ds_hat_r, y_ds_hat_g)

                loss_d = loss_disc_s + loss_disc_f

                loss_d.backward()
                optim_d.step()

                step += 1

                # logging
                if G_train:
                    loss_g = loss_g.item()
                else:
                    loss_g = 0.0
                loss_d = loss_d.item()

                if any([loss_g > 1e8, math.isnan(loss_g), loss_d > 1e8, math.isnan(loss_d)]):
                    logger.error("loss_g %.01f loss_d %.01f at step %d!" % (loss_g, loss_d, step))
                    raise Exception("Loss exploded")

                if step % hp.log.summary_interval == 0:
                    if G_train:
                        writer.log_training(loss_g, loss_d, mel_loss.item(), feat_loss.item(), score_loss.item(), step)
                    else:
                        writer.log_training(0.0, loss_d, 0.0, 0.0, 0.0, step)
                    loader.set_description("g %.04f d %.04f | step %d" % (loss_g, loss_d, step))

            scheduler_g.step()
            scheduler_d.step()

            if epoch % hp.log.validation_interval == 0:
                with torch.no_grad():
                    validate(hp, args, model_g, mpd, msd, valloader, writer, epoch)

            if epoch % hp.log.save_interval == 0:
                save_path = os.path.join(pt_dir, '%s_%s_%04d.pt'
                    % (args.name, githash, epoch))
                torch.save({
                    'model_g': model_g.state_dict(),
                    'mpd': mpd.state_dict(),
                    'msd': msd.state_dict(),
                    'optim_g': optim_g.state_dict(),
                    'optim_d': optim_d.state_dict(),
                    'step': step,
                    'epoch': epoch,
                    'hp_str': hp_str,
                    'githash': githash,
                }, save_path)
                logger.info("Saved checkpoint to: %s" % save_path)

    except Exception as e:
        logger.info("Exiting due to exception: %s" % e)
        traceback.print_exc()
